package com.noah2021.error;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/2/5
 * @return
 */
public interface CommonError {
    public int getErrCode();
    public String getErrMsg();
    public CommonError setErrMsg(String errMsg);
}
