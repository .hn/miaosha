package com.noah2021.service;

import com.noah2021.error.BusinessException;
import com.noah2021.service.model.UserModel;
import org.springframework.stereotype.Service;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/2/5
 * @return
 */
public interface UserService {
    UserModel getUserById(Integer id);
    void register(UserModel userModel) throws BusinessException;
    UserModel validateLogin(String telphone,String encrptPassword) throws BusinessException;
    UserModel getUserByIdInCache(Integer id);
}