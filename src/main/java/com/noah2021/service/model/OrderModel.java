package com.noah2021.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/2/7
 * @return
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
//用户下单的交易模型
public class OrderModel {
    //企业级别的订单号是一个复合型字符串
    private String id;

    //购买的用户id
    private Integer userId;

    //购买的商品id
    private Integer itemId;

    //若非空，则表示是以秒杀商品方式下单
    private Integer promoId;

    //购买商品的单价,当商品单价改变，该变量也会改变。若promoId非空，则表示秒杀商品价格
    private BigDecimal itemPrice;

    //购买数量
    private Integer amount;

    //购买总金额,若promoId非空，则表示秒杀商品价格
    private BigDecimal orderPrice;
}
