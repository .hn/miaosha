package com.noah2021.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/2/7
 * @return
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserModel implements Serializable {
    private Integer id;
    @NotNull(message = "用户名不能为空")
    private String name;
    @NotNull(message = "性别不能为空")
    private Byte gender;
    @NotNull(message = "年龄不能为空")
    @Min(value = 0, message = "年龄必须大于0")
    @Max(value = 200, message = "年龄必须小于200")
    private Integer age;
    @NotNull(message = "手机号不能为空")
    private String telphone;
    private String registerMode;
    private String thirdPartyId;
    @NotNull(message = "密码不能为空")
    private String encrptPassword;
}
